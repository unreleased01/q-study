<div class="ss_student_board">
    <div class="ss_s_b_top">
        <div class="col-sm-6 ss_index_menu">
            <a href="student_assk_list.php">Index</a>
        </div>
        <div class="col-sm-6 ss_next_pre_top_menu">
            <a class="btn btn_next" href="#"><i class="fa fa-caret-left" aria-hidden="true"></i> Back</a>
            <a class="btn btn_next" href="#"><i class="fa fa-caret-right" aria-hidden="true"></i> Next</a>
            <a class="btn btn_next" href="#">Draw <img src="assets/images/icon_draw.png"></a>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="ss_s_b_main" style="min-height: 100vh">
                <div class="col-sm-4">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"> <span><img src="assets/images/icon_draw.png"> Instruction</span> Question</a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class="image_q_list">

                                        <div class="row">
                                            <div class="col-xs-4">Word</div>
                                            <div class="col-xs-8">?</div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4">Definition</div>
                                            <div class="col-xs-8"><?php echo $question_info->definition;?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4">Parts of speech</div>
                                            <div class="col-xs-8"><?php echo $question_info->parts_of_speech;?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4">Synonym </div>
                                            <div class="col-xs-8"><?php echo $question_info->synonym;?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4">Antonym</div>
                                            <div class="col-xs-8"><?php echo $question_info->antonym;?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4">Your Sentence</div>
                                            <div class="col-xs-8"><?php echo $question_info->sentence;?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4">Near Antonym</div>
                                            <div class="col-xs-8"><?php echo $question_info->near_antonym;?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4">Audio File</div>
                                            <div class="col-xs-8"><img src="assets/images/aa.png"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4">Video file</div>
                                            <div class="col-xs-8"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
                <div class="col-sm-4">
                    <div class="panel-group" id="saccordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#saccordion" href="#collapseTow" aria-expanded="true" aria-controls="collapseOne">   Answare</a>
                                </h4>
                            </div>
                            <div id="collapseTow" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class="image_box_list_result result">
                                        <form>
                                            <div class="image_box_list">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <p class="ss_lette"> A </p>
                                                    </div>
                                                    <div class="col-sm-10">
                                                        <div class="box">
                                                            <?php foreach ($question_info->vocubulary_image as $row){?>
                                                            <div class="result_board">
                                                                <?php echo $row?>
                                                            </div>
                                                            <br/>
                                                            <?php }?>
                                                            <div class="form-group">

                                                                <input type="text" class="form-control" id="exampleInputl1" placeholder="result">
                                                            </div>
                                                        </div>
                                                        <div class="letter_box">
                                                            <ul>
                                                                <?php foreach (range('A', 'Z') as $char) {?>
                                                                    <li> <a href=""><?php  echo $char;?></a> </li>
                                                                <?php } ?>
                                                                <li> <a href=""><img src="assets/images/icon_l_d.png"></a> </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <a href=""  class="btn btn_next"  data-toggle="modal" data-target="#ss_info_sucesss" >Submit</a></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="panel-group" id="raccordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#taccordion" href="#collapsethree" aria-expanded="true" aria-controls="collapseOne">  <span>Module Name: Every Sector</span></a>
                                </h4>
                            </div>
                            <div id="collapsethree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class=" ss_module_result">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>    
                                                    <tr>
                                                        <th></th>
                                                        <th>SL</th>
                                                        <th>Mark</th>
                                                        <th>Obtained</th>
                                                        <th>Description</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td> </td>
                                                        <td>1</td>
                                                        <td>5.0</td>
                                                        <td>5.0</td>
                                                        <td><a href="#" class="text-center"><img src="assets/images/icon_details.png"></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td> </td>
                                                        <td>2</td>
                                                        <td>5.0</td>
                                                        <td>5.0</td>
                                                        <td><a href="#" class="text-center"><img src="assets/images/icon_details.png"></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td> </td>
                                                        <td>3</td>
                                                        <td>5.0</td>
                                                        <td>5.0</td>
                                                        <td><a href="#" class="text-center"><img src="assets/images/icon_details.png"></a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

                <div class="col-sm-4" id="draggable">
                    <div class="panel-group" id="waccordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#waccordion" href="#collapseworkout" aria-expanded="true" aria-controls="collapseworkout">  Workout</a>
                                </h4>
                            </div>
                            <div id="collapseworkout" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    sds
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="modal fade ss_modal" id="ss_info_sucesss" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
            </div>
            <div class="modal-body row">
                <img src="assets/images/icon_sucess.png" class="pull-left"> <span class="ss_extar_top20">Your answer is correct</span> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn_blue" data-dismiss="modal">Ok</button>

            </div>
        </div>
    </div>
</div>

<div class="modal fade ss_modal" id="ss_info_worng" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
            </div>
            <div class="modal-body row">
                <i class="fa fa-close" style="font-size:20px;color:red"></i> <span class="ss_extar_top20">Your answer is wrong</span>
                <br><?php // echo strip_tags($question_info[0]['questionName']); ?> = <?php // echo $question_info[0]['answer']; ?> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn_blue" data-dismiss="modal">close</button>         
            </div>
        </div>
    </div>
</div>

<script>
    $('#answer_matching').click(function () {
        var user_answer = CKEDITOR.instances.answer.getData();
        var id = $('#question_id').val();
        $.ajax({
            type: 'POST',
            url: 'answer_matching',
            data: {
                user_answer: user_answer,
                id: id
            },
            dataType: 'html',
            success: function (results) {
                if (results == 0) {
                    $('#ss_info_worng').modal('show');
                } else if (results == 1) {
                    $('#ss_info_sucesss').modal('show');
                }
            }
        });

    });
</script>