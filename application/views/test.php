<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
<!-- bootstrap -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<link 
  href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css" 
  rel="stylesheet"  type='text/css'>
	<!-- context menu -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="../assets/css/context-menu.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.0/jquery.contextMenu.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.0/jquery.ui.position.js"></script>
</head>
<body>
	<ul class="the-node">
    <li id="test"><span class="context-menu-one btn btn-neutral fa fa-eye">right click me 1</span></li>
    <li><span class="context-menu-one btn btn-neutral">right click me 2</span></li>
    <li>right click me 3</li>
    <li>right click me 4</li>
</ul>

<ul class="the-node">
    <li id="test"><span class="context-menu-one btn btn-neutral fa fa-eye">right click me 1</span></li>
    <li><span class="context-menu-one btn btn-neutral">right click me 2</span></li>
    <li>right click me 3</li>
    <li>right click me 4</li>
</ul>
</body>

<script>

/*$.contextMenu({
	selector: '#test',
	callback: function(key, options){
		alert('click1 hit');
		return false;
	}
})*/	
	
$(function(){
    $('.the-node').contextMenu({
        selector: 'li', 
        callback: function(key, options) {
            var m = "clicked: " + key + " on " + $(this).text();
            //window.console && console.log(m) || alert(m);
            if(key=='edit'){
            	console.log('edit clicked');
            }
        },
        items: {
            "edit": {name: "Edit", icon: "fa-certificate"},
            "cut": {name: "Cut", icon: "cut"},
            "copy": {name: "Copy", icon: "copy"},
            "paste": {name: "Paste", icon: "paste"},
            "delete": {name: "Delete", icon: "delete"},
            "sep1": "---------",
            "quit": {name: "Quit", icon: function($element, key, item){ 
            	console.log(':'+key+':'); }
            }
        }
    });
});
</script>
</html>
