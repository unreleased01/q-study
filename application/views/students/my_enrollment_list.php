			<div class="top100">
				<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-10">
				

				<div class="ss_enrollment_list">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          My Enrollment list
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
      	<div class="button_schedule text-right" >
			 
				<a href="c.schedule.php" class="btn btn_next"><i class="fa fa-home"></i> Home</a>
				<a href="" class="btn btn_next"><i class="fa fa-edit" aria-hidden="true"></i> Edit</a>
				</div>
       <div class="ss_enrollment_list_content">
       		<div class="ss_enrollment_list_top">
       			<div class="col-sm-7">Tutor/School</div>
       			<div class="col-sm-3">Ref.Link Number</div>
       			<div class="col-sm-2">Set Link</div>
       		</div>
       		<div class="ss_enrollment_list_mid">
       			<ul>
       				<li class="dropdown">
       					  <div class="dropdown-toggle" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
       					<div class="col-sm-7"><i class="fa fa-caret-right" aria-hidden="true"></i> Tutor</div>
		       			<div class="col-sm-3"> </div>
		       			<div class="col-sm-2 text-center"><a href=""data-toggle="modal" data-target="#ss_enrollment_model"><i class="fa fa-file-o" aria-hidden="true"></i></a></div>
		       			</div>
		       			<ul class="dropdown-menu">
							<?php foreach($get_involved_teacher as $single_tutor_ref){ ?>
								<li>
									<div class="col-sm-7"><?php echo $single_tutor_ref['name'];?></div>
									<div class="col-sm-3"><?php echo $single_tutor_ref['SCT_link'];?></div>
									<div class="col-sm-2 text-center"></div>
								</li>
							<?php } ?>		       				
		       			</ul>
       				</li>
       				<li class="dropdown">
       					  <div class="dropdown-toggle" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
       					<div class="col-sm-7"><i class="fa fa-caret-right" aria-hidden="true"></i>School</div>
		       			<div class="col-sm-3"> </div>
		       			<div class="col-sm-2 text-center"><a href=""data-toggle="modal" data-target="#ss_enrollment_model"><i class="fa fa-file-o" aria-hidden="true"></i></a></div>
		       			</div>
		       			<ul class="dropdown-menu">
		       				<?php foreach($get_involved_school as $single_school_ref){ ?>
								<li>
									<div class="col-sm-7"><?php echo $single_school_ref['name'];?></div>
									<div class="col-sm-3"><?php echo $single_school_ref['SCT_link'];?></div>
									<div class="col-sm-2 text-center"></div>
								</li>
							<?php } ?>	
		       			</ul>
       				</li>
       				<li class="dropdown">
       					  <div class="dropdown-toggle" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
       					<div class="col-sm-7"><i class="fa fa-caret-right" aria-hidden="true"></i> Corporate</div>
		       			<div class="col-sm-3"> </div>
		       			<div class="col-sm-2 text-center"><a href=""data-toggle="modal" data-target="#ss_enrollment_model"><i class="fa fa-file-o" aria-hidden="true"></i></a></div>
		       			</div>
		       			<ul class="dropdown-menu">
		       				<?php foreach($get_involved_corporate as $single_corporate_ref){ ?>
								<li>
									<div class="col-sm-7"><?php echo $single_corporate_ref['name'];?></div>
									<div class="col-sm-3"><?php echo $single_corporate_ref['SCT_link'];?></div>
									<div class="col-sm-2 text-center"></div>
								</li>
							<?php } ?>	
		       			</ul>
       				</li>
       				
       			</ul>
       		</div>
       </div>
      </div>
    </div>
  </div>
  
   
</div>
				
				</div>
			 </div>
			 </div>
			 </div>
		</div>
</section>

<div id="ss_enrollment_model"  class="modal fade ss_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
    <div class="modal-content">
		<form id="add_link_ref_form">
      <div class="modal-header"> 
        <h4 class="modal-title" id="myModalLabel">Link Ref. Number</h4>
      </div>
      <div class="modal-body">
             <div class="form-group  row">
		    <label for="inputPassword3" class="col-sm-6 control-label">ref. Link Number</label>
		    <div class="col-sm-6">
		      <input type="text" class="form-control" id="inputPassword3" value="" name="link[]">
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="inputPassword3" class="col-sm-6 control-label"></label>
		    <div class="col-sm-6">
		      <input type="text" class="form-control" id="inputPassword3" value="" name="link[]">
		    </div>
		  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn_blue" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn_blue" id="add_link_button">Ok </button>
      </div>
			</form>
    </div>
  </div>
</div>
<div id="ss_enrollment_add_link"  class="modal fade ss_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header"> 
        <h4 class="modal-title" id="myModalLabel">Adding Link</h4>
      </div>
      <div class="modal-body">
      		<div class="row">
      		 <br/>
          <img src="assets/images/icon_info.png" class="pull-left"><span class="ss_extar_top20">Sucessfully added link</span> 	

<br/><br/>
                    </div>	 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn_blue" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn_blue">Ok </button>
      </div>
    </div>
  </div>
</div>
<script>
	$('#add_link_button').click(function(){
		var data=$('#add_link_ref_form').serialize();
		$.ajax({
			type: 'ajax',
			method: 'post',
			async: false,
			dataType:'json',
			url: 'save_ref_link',
			data:data,
			success: function(msg){
				if(msg==1){
					alert('ok');
				}	
			}
		});	
	});
</script>