<input type="hidden" name="questionType" value="8">
<input type="hidden" name="questionId" value="<?php echo $qId; ?>">
<div>

    <div class="row">
        <div class="ss_question_add">
            <div class="ss_s_b_main" style="min-height: 100vh">
                <div class="col-sm-4">
                    <div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne1">
                                <h4 class="panel-title">
                                   <a role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne">  Question </a>
                                </h4>
                            </div>
                           <div id="collapseOne1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne1">
                                <div class="panel-body">
                                    <textarea class="form-control" name="question_body" ><?php echo $questionBody; ?></textarea>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

               

                <div class="col-sm-4">

                    <div class="panel-group" id="raccordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#taccordion" href="#collapsefour" aria-expanded="true" aria-controls="collapseOne">  <span>Module Name: Will Dynamic Later</span></a>
                                </h4>
                            </div>
                            <div id="collapsefour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">

                                    <div class=" ss_module_result">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>    
                                                    <tr>

                                                        <th>SL</th>
                                                        <th>Mark</th>
                                                        <th>Obtain</th>
                                                        <th>Description</th>

                                                    </tr>
                                                </thead>
                                                <tbody id="assListTbl">
                                                    <?php echo $assignment_list; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end col -->
                
            </div>
        </div>
    </div>
</div>
</div>
</section>


<!-- Modal -->
<div class="modal fade ss_modal" id="ss_sucess_mess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
            </div>
            <div class="modal-body row">
               <img src="assets/images/icon_info.png" class="pull-left"> <span class="ss_extar_top20">Save Sucessfully</span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn_blue" data-dismiss="modal">Ok</button>

            </div>
        </div>
    </div>
</div>

<!-- question details modal -->
<div class="modal fade" id="quesDtlsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="exampleModalLabel">Assignment Details</h5>
            </div>
            <div class="modal-body">
                <form>

                    <div class="form-group">
                        <label for="quesDesc" class="text-muted">Description</label>
                        <input class="form-control" id="quesDescFromMod" value="" ></input>
                        <input class="form-control" type="hidden" id="quesSlOnMod" value="">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" id="quesDescSubmitBtn" class="btn btn-primary">OK</button>
            </div>
        </div>
    </div>
</div>

<script>

    /*set question serial on description modal*/
    $(document.body).on('click','.qDtlsOpenModIcon', function(){
        var quesSl = $(this).closest('tr').attr('id');
        var hiddenTaskDesc =  $('tr#'+quesSl).find('input#hiddenTaskDesc').val();
        $('#quesDescFromMod').val(hiddenTaskDesc);
        console.log(hiddenTaskDesc);
        $('#quesSlOnMod').val(quesSl)
    });
    /*set question description on hidden input field*/
    $(document.body).on('click','#quesDescSubmitBtn', function(){
        var quesSlFromMod = $('#quesSlOnMod').val();
        var quesDescFromMod = $('#quesDescFromMod').val();
        var hiddenQuesDesc =  $('tr#'+quesSlFromMod).find('input#hiddenTaskDesc').val(quesDescFromMod);
        $('#quesDtlsModal').modal('toggle');
    })

</script>
