<input type="hidden" name="questionType" value="3">
<div class="row">
    <div class="ss_question_add">
        <div class="ss_s_b_main" style="min-height: 100vh">

            <div class="col-sm-4">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><span><img src="assets/images/icon_solution.png"> Solution</span> Question</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body form-horizontal">
                                <!--  <form class="form-horizontal ss_image_add_form"> -->
                                <div class="form-group">
                                    <label for="inputwordl3" class="col-sm-4 control-label">Word</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="inputword3" name="word" placeholder="Word" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputDefinitionl3" class="col-sm-4 control-label">Definition</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="inputDefinitionl3" name="definition" placeholder="Definition" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPartsofspeech3" class="col-sm-4 control-label">Parts of speech</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="inputPartsofspeech3" name="parts_of_speech" placeholder="Parts of speech" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputSynonym3" class="col-sm-4 control-label">Synonym</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="inputSynonym3" name="synonym" placeholder="Synonym" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputAntonym3" class="col-sm-4 control-label">Antonym</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="inputAntonym3" name="antonym" placeholder="Antonym" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputYourSentence3" class="col-sm-4 control-label">Your Sentence</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="inputYourSentence3" name="sentence" placeholder="Your Sentence" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputNearAntonym3" class="col-sm-4 control-label">Near Antonym</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="inputNearAntonym3" name="near_antonym" placeholder="Near Antonym">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-4 control-label">Audio File</label>
                                    <div class="col-sm-8">
                                        <input type="file" id="exampleInputFile" name="audioFile">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-4 control-label">Video file</label>
                                    <div class="col-sm-8">
                                        <input type="file" id="exampleInputFilevideo" name="videoFile">
                                    </div>
                                </div>

                                <!--  </form> -->
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-sm-4">
                <div class="panel-group " id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">  Image</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body ss_imag_add_right">

                                <div class="text-center">
                                    <div class="form-group ss_h_mi">
                                        <label for="exampleInputiamges1">How many images</label>
                                        <div class="select">
                                            <select class="form-control select-hidden">
                                                <option>01</option>
                                                <option>02</option>
                                                <option>03</option>
                                                <option>04</option>
                                            </select><div class="select-styled">01</div><ul class="select-options" style="display: none;"><li rel="01">01</li><li rel="02">02</li><li rel="03">03</li><li rel="04">04</li></ul></div>
                                        </div>
                                    </div>

                                    <div class="image_box_list">
                                        <div class="row">
                                            <div class="col-xs-2">
                                                <p class="ss_lette"> A </p>
                                            </div>
                                            <div class="col-xs-10">
                                                <div class="box">
                                                    <textarea class="form-control"></textarea>
                                                </div>
                                            </div>
                                        </div> 

                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="col-sm-4">

                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Answer</a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <input type="text" class="form-control" name="answer">
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
            </form> <!-- form end -->
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade ss_modal" id="ss_sucess_mess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
            </div>
            <div class="modal-body row">
                <img src="assets/images/icon_info.png" class="pull-left"> <span class="ss_extar_top20">Save Sucessfully</span> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn_blue" data-dismiss="modal">Ok</button>

            </div>
        </div>
    </div>
</div>

<script>
    $('.make_table').click(function(e){
        e.preventDefault();
        $('.dynamic_table_skpi_tbody').html('');
        var htm = '';
        var numOfrow = $('#trows_number').val();
        var numOfcol = $('#tcolumns_number').val();
        for(i=1;i<=numOfrow;i++){
            htm += '<tr class="rw"'+i+'>';
            for(j=1;j<=numOfcol;j++){
                htm += '<td><input type="text" data_q_type="0" data_num_colofrow="'+i+'_'+j+'" value="" name="skip_counting[]" class="input-box rsskpin rsskpinpt'+i+'_'+j+'" readonly></td>';
            }
            htm += '</tr>';
        }
        $('.dynamic_table_skpi_tbody').html(htm);
    })
</script>

