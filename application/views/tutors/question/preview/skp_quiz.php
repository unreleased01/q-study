<input type="hidden" name="questionType" value="6">

<div>

    <div class="row">
        <div class="ss_question_add">
            <div class="ss_s_b_main" style="min-height: 100vh">
                <div class="col-sm-4">
                    <div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <?php echo $questionBody; ?>
                            </div>
                            <div class="panel-body">
                                <?php if ($this->session->userdata('wrong_ans')) : ?>
                                    <div class="alert alert-danger" role="alert">
                                        Wrong Answer Given
                                        <button class='btn btn-default btn-sm' data-toggle="modal" data-target="#rightAns" id="rightAnsBtn">Answer</button>
                                    </div>
                                <?php elseif ($this->session->userdata('right_ans')) : ?>
                                    <div class="alert alert-success" role="alert">
                                        Congres right answer given
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>

                    </div>
                </div>


                <div class="col-sm-3 myNewDivHeight" >
                    <div class="skip_box">
                        <form action="Tutor/checkSkpboxAnswer" method="post" name="ansForm">
                            <input type="hidden" id="questionId" name="questionId" value="<?php echo $questionId; ?>">
                            <div class="table-responsive">
                                <table class="dynamic_table_skpi table table-bordered">
                                    <tbody class="dynamic_table_skpi_tbody">
                                        <?php echo $skp_box; ?>
                                    </tbody>
                                </table>
                                <!-- may be its a draggable modal -->
                                <div id="skiping_question_answer" style="display:none">
                                    <input type="text" name="set_skip_value" class="input-box form-control rs_set_skipValue">
                                </div>
                            </div>
                            <div>
                                <input type="submit" class="btn btn-primary btn-sm" value="Submit">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</section>


<!-- Modal -->
<div class="modal fade ss_modal" id="ss_sucess_mess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
            </div>
            <div class="modal-body row">
                <img src="assets/images/icon_info.png" class="pull-left"> <span class="ss_extar_top20">Save Sucessfully</span> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn_blue" data-dismiss="modal">Ok</button>

            </div>
        </div>
    </div>
</div>

<!-- right ans modal -->
<div class="modal fade" id="rightAns" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Correct Answer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" >
                <table class="dynamic_table_skpi table table-bordered">
                    <tbody class="dynamic_table_skpi_tbody" id="rightAnsTblBody">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script>

    $(function() {
        $( "#draggable" ).draggable();
    });

    /*pick given input, make obj , set on hidden field*/
    $('.ans_input').on('input', function(){
        var ansElem = $(this);
        var val = $(this).val();
        var type = 'a';
        var colOfRow = $(this).attr('data_num_colofrow');
        var obj = {cr:colOfRow, val:val, type:type}
        var jsonString = JSON.stringify(obj);
        $(this).siblings('#given_ans').val(jsonString);
    });

    /*append the right ans to the modal body*/
    $('#rightAnsBtn').on('click', function(){
        var questionId = $('#questionId').val();
        $.ajax({
            url:'Tutor/getRightAns',
            method: 'POST',
            data:{qId:questionId},
            success: function(data){
                console.log(data);
                $('#rightAnsTblBody').html(data);
            }
        })
    });
    
    function fn_check(aval){
        $("#answer").val(aval);
    }
</script>
