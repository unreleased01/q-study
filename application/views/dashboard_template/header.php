<div class="sign_up_header">
    <div class="col-sm-4">
        <div class="col-md-12">
            <span style="float: left;">Welcome <?php echo $this->session->userdata('user_email'); ?></span>
            
            <a href="logout" class="col-xs-2">
                <button class="btn btn-default" style="color: #6e8de7;padding: 3px 12px;background-color: #ededed;border-color: #ccc;font-weight: bold;">Logout</button>
            </a>
            
        </div>
        <div class="col-md-12" style="margin-top: 5px;">
             <?php if(isset($user_info[0]['image'])){ ?>   
            <?php  if(file_exists(base_url().'assets/uploads/'.$user_info[0]['image']) ){ ?> 							
            <img src="<?php echo base_url();?>assets/uploads/<?php echo $user_info[0]['image'];?>" alt="User Image" style="width: 130px;"/>	<?php } ?>			
            <?php }else{ ?>
            <img src="assets/images/default_user.jpg" alt="User Image" style="width: 130px;"/>
            <?php } ?>

        </div>
    </div>
    <div class="col-sm-4 text-center">
        <a href="#"><img src="assets/images/logo_signup.png"></a>
    </div>
    <div class="col-sm-4">
        <div class="top_signup">
            <ul>
                <li><a href="#">Back</a></li>
                <li><a href="#"><img src="assets/images/icon_video.png"/> Video Help </a><span>1</span></li>
            </ul>
        </div>
    </div>
</div>