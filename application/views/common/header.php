<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>.:: Q-Study :: Tutor yourself...</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.5">
    <!-- Framework Css -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 
    <!-- Font Awesome / Icon Fonts -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/lib/font-awesome.min.css">
    <!-- Owl Carousel / Carousel- Slider -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/lib/owl.carousel.min.css">
    <!-- Animations -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/lib/animations.min.css">
    <!-- Style Theme -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
    <!-- Light Style Theme -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/light.css">
    <!-- Responsive Theme -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/responsive.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/countrySelect.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/intlTelInput.min.css">
    <!-- Stripe JavaScript library -->
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>

    
</head>
<body>
<div class="wrapper">
    <!--===================== Header ========================-->
