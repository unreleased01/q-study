<?php
defined('BASEPATH') or exit('No direct script access allowed');
class PaypalController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
    }
    public function paypal_notify()
    {
      
        //mail("ai.shobujice@gmail.com","My subject","Message Description");
      
        $req = 'cmd=_notify-validate';
        //mail("ai.shobujice@gmail.com","My subject","Message Description");
        foreach ($_POST as $key => $value) {
            $value = urlencode(stripslashes($value));
            $value = preg_replace('/(.*[^%^0^D])(%0A)(.*)/i', '${1}%0D%0A${3}', $value);
            $req .= "&$key=$value";
        }

        reset($_POST);
        $datas = print_r($_POST, true);
        //mail("ai.shobujice@gmail.com","My subject",$datas);
        //die();
      /* save payer info to database */

        //$data['UserId'] = $_POST['custom'];
        $userId_courseId=explode(',', $_POST['custom']);
        $data['user_id']=$userId_courseId[0];
        $data['PaymentDate'] = time();
        $paymentType = $userId_courseId[1];
        if ($paymentType == 1) {
            $second = 30 * 24 * 3600;
        } elseif ($paymentType == 2) {
            $second = 30 * 6 * 24 * 3600;
        } elseif ($paymentType == 3) {
            $second = 30 * 12 * 24 * 3600;
        }
        $data['PaymentEndDate'] = $data['PaymentDate'] + $second;
        $data['total_cost'] = $_POST['mc_gross'];
         // $data['PackageId'] = $_POST['item_number'];
        $data['payment_status'] = $_POST['payment_status'];
        $data['SenderEmail'] = $_POST['payer_email'];
        $data['paymentType'] =2;
        array_shift($userId_courseId);
        array_shift($userId_courseId);
 
         // $package_info = $this->SiteModel->get_all_info_by_id('tbl_package','PackageId',$data['PackageId']);
 
        $instra = print_r($data, true);
 
 
        $ch = curl_init('https://ipnpb.sandbox.paypal.com/cgi-bin/webscr');
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
 
        if (!($res = curl_exec($ch))) {
            // error_log("Got " . curl_error($ch) . " when processing IPN data");
              curl_close($ch);
              exit;
        }
        curl_close($ch);

        // inspect IPN validation result and act accordingly
        if (strcmp($res, "VERIFIED") == 0) {
                $this->db->insert('tbl_payment', $data);
                $paymentId=$this->db->insert_id();
            if ($userId_courseId) {
                foreach ($userId_courseId as $dacourseId) {
                       $pay['paymentId']=$paymentId;
                       $pay['courseId']=$dacourseId;
                       $this->db->insert('tbl_payment_details', $pay);
                }
            }
   
   

            $instra = print_r($data, true);
            //$notification_msg = 'Your Subscription with the Payment of $' .$_POST['mc_gross']. ' for the Package of '.$package_info[0]['PackageName'].' is complete';
   
            $this->db->set('payment_status', $data['payment_status']);
            $this->db->where('id', $data['user_id']);
            $this->db->update('tbl_useraccount');
           // mail("ai.shobujice@gmail.com","My subject",$this->db->last_query());
            mail("ai.shobujice@gmail.com", "My subject", $second);
   
           // mail($_POST['payer_email'], "access tbl insert", $notification_msg, "From: info@dreamwithme.com");
   
            // IPN message values depend upon the type of notification sent.
            // To loop through the &_POST array and print the NV pairs to the screen:
            foreach ($_POST as $key => $value) {
                echo $key . " = " . $value . "<br>";
            }
        } elseif (strcmp($res, "INVALID") == 0) {
                 // $notification_msg = 'An Error is occured during payment for the package of '.$package_info[0]['PackageName'];
            // IPN invalid, log for manual investigation
                 // mail($_POST['payer_email'], "access tbl insert",$notification_msg , "From: info@dreamwithme.com");
      //            echo "The response from IPN was: <b>" . $res . "</b>";
        }

        header("HTTP/1.1 200 OK");
        //return true;
    }


    public function paypalRetrunNotification()
    {
        $data['pagetitle'] = 'Cancel Payment';
        $data['user_id'] = $_SESSION['user_id'];
    
    
        $this->load->view('admin_template/headerlink', $data);
        $this->load->view('admin_template/header');
        $this->load->view('lawyer/payment/success_payment_form', $data);
        $this->load->view('admin_template/footerlink');
    }

    public function paypalCancelNotification()
    {
        $data['pagetitle'] = 'Cancel Payment';
        $data['user_id'] = $_SESSION['user_id'];
    
    
        $this->load->view('admin_template/headerlink', $data);
        $this->load->view('admin_template/header');
        $this->load->view('lawyer/payment/cancel_payment_form', $data);
        $this->load->view('admin_template/footerlink');
    }
}
