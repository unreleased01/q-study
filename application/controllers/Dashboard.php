<?php


class Dashboard extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $user_id   = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('userType');
        if ($user_id == null && $user_type == null) {
            redirect('welcome');
        }

    }//end __construct()


    public function index()
    {
        // echo $this->session->userdata('user_type');die;
        if ($this->session->userdata('userType') == 1) {
            redirect('parents');
        }

        if ($this->session->userdata('userType') == 2) {
            redirect('upper_level');
        }

        if ($this->session->userdata('userType') == 3) {
            redirect('tutor');
        }

        if ($this->session->userdata('userType') == 4) {
            redirect('school');
        }

        if ($this->session->userdata('userType') == 5) {
            redirect('corporate');
        }

        if ($this->session->userdata('userType') == 6) {
            redirect('student');
        }

    }//end index()


}//end class
