<?php

class Student extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('userType');
        if ($user_id == NULL && $user_type == NULL) {
            redirect('welcome');
        }
        $this->load->model('Student_model');
        $this->load->model('tutor_model');
         $this->load->model('Preview_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('upload');
    }

    public function index() {
        $data['user_info'] = $this->Student_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));

        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        
        $data['maincontent'] = $this->load->view('students/students_dashboard', $data, TRUE);
        $this->load->view('master_dashboard', $data);
    }

    public function student_setting() {
        $data['user_info'] = $this->Student_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        
        $data['maincontent'] = $this->load->view('students/student_setting', $data, TRUE);
        $this->load->view('master_dashboard', $data);
    }

    public function student_details() {
        $data['user_info'] = $this->Student_model->userInfo($this->session->userdata('user_id'));
        $data['studentRefLink'] = $this->Student_model->getStudentRefLink($this->session->userdata('user_id'));
        
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        
        $data['maincontent'] = $this->load->view('students/student_details', $data, TRUE);
        $this->load->view('master_dashboard', $data);
    }

    public function update_student_details() {
        $this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[6]|min_length[5]');
        $this->form_validation->set_rules('passconf', 'passconf', 'trim|required|matches[password]');
        if ($this->form_validation->run() == false) {
            echo 0;
        } else {
            $password = md5($this->input->post('password'));
            $data = array(
                'user_pawd' => $password
            );
            $this->Student_model->updateInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'), $data);
            echo 1;
        }
    }

    public function my_enrollment() {
        $data['user_info'] = $this->Student_model->userInfo($this->session->userdata('user_id'));
        $data['get_involved_teacher'] = $this->Student_model->get_sct_enrollment_info($this->session->userdata('user_id'), 3);
        $data['get_involved_school'] = $this->Student_model->get_sct_enrollment_info($this->session->userdata('user_id'), 4);
        $data['get_involved_corporate'] = $this->Student_model->get_sct_enrollment_info($this->session->userdata('user_id'), 5);
        
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        
        $data['maincontent'] = $this->load->view('students/my_enrollment_list', $data, TRUE);
        $this->load->view('master_dashboard', $data);
    }

    public function save_ref_link() {
        $data_link = $this->input->post('link');
        if (!empty($data_link)) {
            $userType = $this->input->post('userType');
            $j = 0;
            foreach ($data_link as $single_link) {
                if ($single_link) {
                    $get_link_validate = $this->Student_model->getLinkInfo('tbl_useraccount', 'SCT_link', 'user_type', $single_link, $userType);
                    if (!$get_link_validate) {
                        $j++;
                    }
                }
            }
            if ($j > 0) {
                echo 2;
            } else {
                $this->Student_model->delete_enrollment($userType, $this->session->userdata('user_id'));
                foreach ($data_link as $single_link) {
                    $get_link_status = $this->Student_model->getLinkInfo('tbl_useraccount', 'SCT_link', 'user_type', $single_link, $userType);

                    if ($get_link_status) {
                        $enrollment_info = $this->Student_model->getLinkInfo('tbl_enrollment', 'sct_id', 'st_id', $get_link_status[0]['id'], $this->session->userdata('user_id'));
                        //echo '<pre>';print_r($get_link_status);die;
                        if (!$enrollment_info) {

                            $link['sct_id'] = $get_link_status[0]['id'];
                            $link['sct_type'] = $get_link_status[0]['user_type'];
                            $link['st_id'] = $this->session->userdata('user_id');
                            $this->Student_model->insertInfo('tbl_enrollment', $link);
                        }
                    }
                }

                echo 1;
            }
        } else {
            echo 0;
        }
    }

    public function get_ref_link() {
        $user_type = $this->input->post('user_type');
        $st_id = $this->session->userdata('user_id');
        $enrollment_info = $this->Student_model->get_sct_enrollment_info($st_id, $user_type);
        echo json_encode($enrollment_info);
    }

    public function student_upload_photo() {
        $data['user_info'] = $this->Student_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        
        $data['maincontent'] = $this->load->view('students/upload', $data, TRUE);
        $this->load->view('master_dashboard', $data);
    }

    private function upload_user_photo_options() {
        $config = array();
        $config['upload_path'] = './assets/uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        // $config['max_width'] = 1080;
        // $config['max_height'] = 640;
        // $config['min_width']  = 150;
        // $config['min_height'] = 150;
        $config['overwrite'] = FALSE;
        return $config;
    }

    public function sure_student_photo_upload() {
        $this->upload->initialize($this->upload_user_photo_options());
        if (!$this->upload->do_upload('file')) {
            echo 0;
        } else {
            $imageName = $this->upload->data();
            $user_profile_picture = $imageName['file_name'];
            $data = array(
                'image' => $user_profile_picture
            );
            $rs['res'] = $this->Student_model->updateInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'), $data);
            echo 1;
        }
    }
    
    public function view_course() {
        $data['user_info'] = $this->Student_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('students/student_course/view_course', $data, TRUE);
        $this->load->view('master_dashboard', $data);
    }
    
    public function q_study_course() {
        $data['user_info'] = $this->Student_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['tutor_type'] = 7;
        
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('students/student_course/q_study_course', $data, TRUE);
        $this->load->view('master_dashboard', $data);
    }
    
    public function all_module_by_type($tutorType,$module_type) {
        $user_country = $this->session->userdata('country_id');
        $data['user_info'] = $this->Student_model->userInfo($this->session->userdata('user_id'));
        $data['subject_info'] = $this->Student_model->subjectInfo($tutorType);
        
//        echo '<pre>';print_r($data['subject_info']);die;
        
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        if($tutorType == 7) {
            $data['all_module'] = $this->Student_model->all_module_by_type($tutorType,$module_type);
            $data['maincontent'] = $this->load->view('students/tutorial/all_tutorial_list', $data, TRUE);
        }
        if($tutorType == 3) {
            $data['sct_info'] = $this->Student_model->get_all_tutor_link_with_module($this->session->userdata('user_id'),$module_type);
            foreach ($data['sct_info'] as $row){
                $sct_info[$row['name']][] = $row;
//                $sct_info[] = $sct_info[$row['sct_id']];
            }
            $data['module_info'] = $sct_info;
//            echo '<pre>';print_r($data['module_info']);die;
            $data['maincontent'] = $this->load->view('students/today_task/tutor_list', $data, TRUE);
        }
        
        $this->load->view('master_dashboard', $data);
    }
    
    public function tutorial($tutorType) {
        $user_country = $this->session->userdata('country_id');
        $data['user_info'] = $this->Student_model->userInfo($this->session->userdata('user_id'));
        $data['subject_info'] = $this->Student_model->subjectInfo($tutorType);
        
        if($tutorType == 7){
            $data['all_module'] = $this->Student_model->all_module_by_type($user_country,$tutorType);
        }
        if($tutorType == 3){
            $sct_info = $this->Student_model->get_sct_enrollment_info($this->session->userdata('user_id'),$this->session->userdata('userType'));
            $data['all_module'] = $this->Student_model->module_by_type($user_country,$sct_info[0]['sct_id']);
        }
        
//        echo '<pre>';print_r($data['subject_info']);die;
        
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('students/tutorial/all_tutorial_list', $data, TRUE);
        $this->load->view('master_dashboard', $data);
    }
    
    public function tutor_course() {
        $data['user_info'] = $this->Student_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
        $data['tutor_type'] = 3;
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);

        $data['maincontent'] = $this->load->view('students/student_course/q_study_course', $data, TRUE);
        $this->load->view('master_dashboard', $data);
        
//        $data['user_info'] = $this->Student_model->userInfo($this->session->userdata('user_id'));
//        $data['sct_info'] = $this->Student_model->get_all_tutor_link($this->session->userdata('user_id'));
//        
////        echo '<pre>';print_r($data['sct_info']);die;
//        
//        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
//        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
//        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
//
//        $data['maincontent'] = $this->load->view('students/today_task/tutor_list', $data, TRUE);
//        $this->load->view('master_dashboard', $data);
    }
    // added by sobuj
   public function get_tutor_tutorial_module($modle_id,$question_order_id)
    {
    
       //$this->session->unset_userdata('data');die;
       $gurd_array = $this->session->userdata('data');
       
       $this->check_browser_back_next_previlige($gurd_array,$question_order_id);
       
       $data['user_info'] = $this->tutor_model->getInfo('tbl_useraccount', 'id', $this->session->userdata('user_id'));
       $data['question_info_s'] = $this->tutor_model->getModuleQuestion($modle_id,$question_order_id,null);
       $data['total_question'] = $this->tutor_model->getModuleQuestion($modle_id,null,1);
       $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
       
       $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
       $data['header'] = '';
       $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);   
       
       //echo '<pre>';print_r($data['question_info_s']);die;
       if($data['question_info_s'][0]['question_type']==1)
       {    
            $data['maincontent'] = $this->load->view('stude_module_tutor_tutorial/ans_general', $data, TRUE);       
       }
       elseif($data['question_info_s'][0]['question_type']==2)
       {
           $data['maincontent'] = $this->load->view('stude_module_tutor_tutorial/ans_true_false', $data, TRUE);   
       }
       elseif($data['question_info_s'][0]['question_type']==3)
       {
           $data['question_info_vcabulary'] = json_decode($data['question_info_s'][0]['questionName']);   
           $data['maincontent'] = $this->load->view('stude_module_tutor_tutorial/ans_vocabulary', $data, TRUE);   
       }
       elseif($data['question_info_s'][0]['question_type']==4)
       {
           $data['question_info_vcabulary'] = json_decode($data['question_info_s'][0]['questionName']);
           $data['maincontent'] = $this->load->view('stude_module_tutor_tutorial/answ_multiple_choice', $data, TRUE);   
       }
       elseif($data['question_info_s'][0]['question_type']==5)
       {
           $data['question_info_vcabulary'] = json_decode($data['question_info_s'][0]['questionName']);
           $data['maincontent'] = $this->load->view('stude_module_tutor_tutorial/ans_multiple_response', $data, TRUE);   
       } 
       elseif($data['question_info_s'][0]['question_type']==6)
       {
            $quesInfo     = $this->tutor_model->getModuleQuestion($modle_id,$question_order_id,null);
           
            $data['question_info_s']   = $quesInfo;
            $questionType = $quesInfo[0]['questionType'];
            $quesInfo     = json_decode($quesInfo[0]['questionName']);
            $data['question_info_skip'] = json_decode($data['question_info_s'][0]['questionName']);
           
            $data['numOfRows']    = isset($quesInfo->numOfRows) ? $quesInfo->numOfRows : 0;
            $data['numOfCols']    = isset($quesInfo->numOfCols) ? $quesInfo->numOfCols : 0;
            $data['questionBody'] = isset($quesInfo->question_body) ? $quesInfo->question_body : '';
           
            $data['questionId']   = $data['question_info_s'][0]['question_id'];
           $data['question_id']   = $data['question_info_s'][0]['question_id'];
            
//            echo '<pre>';print_r($data['question_info_s']);die;
            $quesAnsItem          = $quesInfo->skp_quiz_box;
            $items = $this->indexQuesAns($quesAnsItem);
            $data['skp_box'] = $this->renderSkpQuizPrevTable($items, $data['numOfRows'], $data['numOfCols']);
           
            $data['maincontent'] = $this->load->view('stude_module_tutor_tutorial/ans_skip', $data, TRUE);   
       } 
       elseif($data['question_info_s'][0]['question_type']==7)
       {
           $data['question_info_left_right'] = json_decode($data['question_info_s'][0]['questionName']); 
           $data['maincontent'] = $this->load->view('stude_module_tutor_tutorial/ans_matching', $data, TRUE);   
       }
       $this->load->view('master_dashboard', $data);
    }
    
    private function check_browser_back_next_previlige($gurd_array,$question_order_id)
    {
            if(is_array($gurd_array))
           {
               if(array_key_exists ( $question_order_id , $gurd_array ))
               {
                   $has_module_type = $gurd_array[$question_order_id]['module_type']; 
                   if($has_module_type)
                   {
                      if($has_module_type == 1)
                      {
                        return;
                      }
                      else
                      {
                        $hasData = $gurd_array[$question_order_id]['question_id'];
                        if($hasData)
                        {
                           $redirect_order_id = $question_order_id + 1;
                           redirect('get_tutor_tutorial_module/'.$gurd_array[$question_order_id]['module_id'].'/'.$redirect_order_id); 
                        }
                        else
                        {
                            return;
                        }

                      }
                   }
               }
           }
           else
           {
               return;
           }
        }
    
    /**
     * before passing items to renderSkpQuizPrevTable() index it first with this func
     * @param  array $items json object array
     * @return array        array with proper indexing
     */
    public function indexQuesAns($items)
    {
        //print_r($items);die;
        $arr = [];
        foreach ($items as $item) {
            $temp = json_decode($item);
            $cr = explode('_', $temp->cr);
            $col = $cr[0];
            $row = $cr[1];
            $arr[$col][$row] = array(
                'type' => $temp->type,
                'val' => $temp->val
                );
        }
        return $arr;
    }
    
    /**
     * render the indexed item to table data for preview
     * @param  array  $items   ques ans as indexed item
     * @param  int  $rows    num of row in table
     * @param  int  $cols    num of cols in table
     * @param  integer $showAns optional, set 1 will show the answers too
     * @return string           table item
     */
    public function renderSkpQuizPrevTable($items, $rows, $cols, $showAns=0)
    {
        //print_r($items);die;
        $row = '';
        for ($i=1; $i<=$rows; $i++) {
            $row .='<tr>';
            for ($j=1; $j<=$cols; $j++) {
                if ($items[$i][$j]['type']=='q') {
                    $row .= '<td><input type="button" data_q_type="0" data_num_colofrow="" value="'.$items[$i][$j]['val'].'" name="skip_counting[]" class="form-control input-box  rsskpinpt'.$i.'_'.$j.'" readonly style="min-width:50px; max-width:50px"></td>';
                } else {
                    $ansObj = array(
                        'cr'=>$i.'_'.$j,
                        'val'=> $items[$i][$j]['val'],
                        'type'=> 'a',
                        );
                    $ansObj = json_encode($ansObj);
                    $val = ($showAns==1)?' value="'.$items[$i][$j]['val'].'"' : '';
                    
                    $row .= '<td><input autocomplete="off" type="text" '.$val.' data_q_type="0" data_num_colofrow="'.$i.'_'.$j.'" value="" name="skip_counting[]" class="form-control input-box ans_input  rsskpinpt'.$i.'_'.$j.'"  style="min-width:50px; max-width:50px">';
                    $row .= '<input type="hidden" value="" name="given_ans[]" id="given_ans">';
                    $row .='</td>';
                }
            }
            $row .= '</tr>';
        }
        
        return $row;
    }
    
    
    private function take_decesion_1($question_id,$module_id,$question_order_id,$text,$text_1,$answer_info=null)
    {
        //echo 'take_decesion_1';die;
        $ans_array = $this->session->userdata('data');
        if(!is_array($ans_array)) $ans_array = array();
        if($text == $text_1){
            if($_POST['next_question'] ==0 )
            {
                $this->save_student_answer($module_id,$_POST['current_order']);
            }
            else
            {
                $question_info_ai = $this->tutor_model->getModuleQuestion($module_id,$question_order_id,null);
                $link1 = base_url();
                $link2 = $link1.'get_tutor_tutorial_module/'.$module_id.'/'.$question_order_id;
                $ind_ans = array( 'question_order_id'=>$question_info_ai[0]['question_order'], 'module_type'=>$question_info_ai[0]['moduleType'], 'module_id'=>$question_info_ai[0]['module_id'],'question_id'=>$question_info_ai[0]['question_id'],'link'=>$link2);
                $ans_array[$question_order_id] = $ind_ans;
                $this->session->set_userdata('data',$ans_array);
                if($answer_info != null)
                {
                    echo $answer_info;
                }
                else
                {
                    echo 2;
                }
                
            }
            
        }else{
            if($answer_info != null)
            {
                echo $answer_info;
            }
            else
            {
                echo 3;
            }
        }
    }
    private function take_decesion_2($question_id,$module_id,$question_order_id,$text,$text_1)
    {
        $ans_array = $this->session->userdata('data');
        if(!is_array($ans_array)) $ans_array = array();
        if($text != $text_1)
        {
            $data['st_id'] = $this->session->userdata('user_id');
            
            $data['question_id'] = $question_id;    
            
            $data['module_id'] = $module_id;
            
            $this->db->insert('tbl_st_error_ans', $data);
        }
        if($_POST['next_question'] ==0 )
        {
            
            $this->save_student_answer($module_id,$_POST['current_order']);
        }
        else
        {
            $question_info_ai = $this->tutor_model->getModuleQuestion($module_id,$question_order_id,null);
            $link1 = base_url();
            $link2 = $link1.'get_tutor_tutorial_module/'.$module_id.'/'.$question_order_id;
            $ind_ans = array( 'question_order_id'=>$question_info_ai[0]['question_order'], 'module_type'=>$question_info_ai[0]['moduleType'], 'module_id'=>$question_info_ai[0]['module_id'],'question_id'=>$question_info_ai[0]['question_id'],'link'=>$link2);
            $ans_array[$question_order_id] = $ind_ans;
            $this->session->set_userdata('data',$ans_array);
            echo 5;
            
        }
                
    }
    public function st_answer_matching(){
        //echo '<pre>';print_r($_POST['module_type']);die;
        $question_id = $_POST['id'];
        $module_id = $_POST['check_module_id'];
        $question_order_id=$_POST['check_order_id']-1;
        $text = $_POST['user_answer'];
        $find = array('&nbsp;','\n','\t','\r');
        $repleace = array('','','','');     
        $text = strip_tags($text);
        $text = str_replace($find,$repleace,$text);
        $text = trim($text);
        
        $answer_info=$this->Preview_model->getInfo('tbl_question','id',$question_id);
        
        $text_1 = $answer_info[0]['answer'];
        $find = array('&nbsp;','\n','\t','\r');
        $repleace = array('','','','');     
        $text_1 = strip_tags($text_1);
        $text_1 = str_replace($find,$repleace,$text_1);
        $text_1 = trim($text_1);
        
        if($_POST['module_type'] == 1)
        {
            $this->take_decesion_1($question_id,$module_id,$question_order_id,$text,$text_1);
        }
        else
        {
            $this->take_decesion_2($question_id,$module_id,$question_order_id,$text, $text_1);
        }
        
    }
    public function st_answer_matching_vocabolary()
    {
        
        $this->form_validation->set_rules('answer', 'answer', 'required');
        
        if($this->form_validation->run() == FALSE){
            echo 1;
        }else{
            $text = strtolower($this->input->post('answer'));
            $question_id=$this->input->post('question_id');
            $answer_info=$this->Preview_model->getInfo('tbl_question','id',$question_id);
            
            $module_id = $_POST['module_id'];
            $question_order_id = $_POST['next_question']-1;
            
            $text_1 = strtolower($answer_info[0]['answer']);
            
            
            
            if($_POST['module_type'] == 1)
            {
                $this->take_decesion_1($question_id,$module_id,$question_order_id,$text,$text_1);
            }
            else
            {
                $this->take_decesion_2($question_id,$module_id,$question_order_id,$text, $text_1);
            }
        }
    }
    public function st_answer_matching_true_false()
    {
        $this->form_validation->set_rules('answer', 'answer', 'required');
        if($this->form_validation->run() == FALSE){
            echo 1;
        }else{
            $text = $this->input->post('answer');
            $question_id=$this->input->post('question_id');
            
            $module_id = $_POST['module_id'];
            $question_order_id=$_POST['next_question']-1;
            
            $answer_info=$this->Preview_model->getInfo('tbl_question','id',$question_id);
            $text_1 = $answer_info[0]['answer'];
            
            if($_POST['module_type'] == 1)
            {
                $this->take_decesion_1($question_id,$module_id,$question_order_id,$text,$text_1);
            }
            else
            {
                $this->take_decesion_2($question_id,$module_id,$question_order_id,$text, $text_1);
            }
        }
    }
    public function st_answer_matching_multiple_choice()
    {
        $question_id = $_POST['id'];
        $text_1 = $_POST['answer_reply'];
        $answer_info=$this->Preview_model->getInfo('tbl_question','id',$question_id);       
        $text = $answer_info[0]['answer'];
        
        $module_id = $_POST['module_id'];
        $question_order_id=$_POST['next_question']-1;
        
        if($_POST['module_type'] == 1)
        {
            $this->take_decesion_1($question_id,$module_id,$question_order_id,$text,$text_1);
        }
        else
        {
            $this->take_decesion_2($question_id,$module_id,$question_order_id,$text, $text_1);
        }
    }
    public function st_answer_matching_multiple_response()
    {
        $question_id = $_POST['id'];
        $text_1 = $_POST['answer_reply'];
        
        $answer_info=$this->Preview_model->getInfo('tbl_question','id',$question_id);       
        //$text = $answer_info[0]['answer'];
        
        $text = json_decode($answer_info[0]['answer']);
        $result_count = count(array_intersect($text_1, $text));
        
        
        $module_id = $_POST['module_id'];
        $question_order_id=$_POST['next_question']-1;
        
        
        if($_POST['module_type'] == 1)
        {
            $this->take_decesion_1($question_id,$module_id,$question_order_id,count($text_1),$result_count);
        }
        else
        {
            $this->take_decesion_2($question_id,$module_id,$question_order_id,count($text),$result_count);
        }
    }
    public function st_answer_multiple_matching()
    {
        $total = $_POST['total_ans'];
        
        $question_id = $_POST['id'];
        $st_ans = array();
        for($i=1; $i <= $total; $i++)
        {
            $ans_id = 'answer_'.$i;
            $st_ans[] = $_POST[$ans_id];
        }
        $answer = $this->Preview_model->getInfo('tbl_question','id',$question_id);
        $answer_info['tutor_ans'] = json_decode($answer[0]['answer']);
        
        $answer_info['student_ans'] = $st_ans;
        
        $module_id = $_POST['module_id'];
        $question_order_id = $_POST['next_question']-1;
        
        $text = 0;
        $text_1 = 0;
        for($k = 0; $k < sizeof($answer_info['student_ans']); $k++)
        {
            if($answer_info['student_ans'][$k] != $answer_info['tutor_ans'][$k])
            {
                $text++;
            }
        }
        
        if($_POST['module_type'] == 1)
        {
            $this->take_decesion_1($question_id,$module_id,$question_order_id,$text,$text_1,json_encode($answer_info));
        }
        else
        {
            $this->take_decesion_2($question_id,$module_id,$question_order_id,$text, $text_1);
        }
        
    }
    
     public function st_answer_skip()
     {
        //echo '<pre>';print_r($_POST);die;
        $module_id = $_POST['module_id'];
        $question_order_id=$_POST['next_question']-1;
        
        $post = $this->input->post();
        $questionId = $_POST['question_id'];//$this->input->post('question_id');
        $givenAns = $this->indexQuesAns($post['given_ans']);

        $temp = $this->tutor_model->getInfo('tbl_question', 'id', $questionId);
        //echo '<pre>';print_r($temp);die;
        $savedAns = $this->indexQuesAns(json_decode($temp[0]['answer']));

        $temp2 = json_decode($temp[0]['questionName']);
        $numOfRows = $temp2->numOfRows;
        $numOfCols = $temp2->numOfCols;
        //echo $numOfRows .' ' . $numOfCols;
        $wrongAnsIndices = [];
        
        $text = 0;
        $text_1 = 0;
        for ($row=1; $row<=$numOfRows; $row++) {
            for ($col=1; $col<=$numOfCols; $col++) {
                if (isset($savedAns[$row][$col])) {
                    $wrongAnsIndices[] = ($savedAns[$row][$col] != $givenAns[$row][$col]) ? $row.'_'.$col:null;
                    
                }
            }
        }
        
        $wrongAnsIndices = array_filter($wrongAnsIndices);
        if (count($wrongAnsIndices)) {//For False Condition
            $text_1 = 1;
        }
        
        if($_POST['module_type'] == 1)
        {
            //echo $text_1;die;
            $this->take_decesion_1($questionId,$module_id,$question_order_id,$text,$text_1);
        }
        else
        {
            $this->take_decesion_2($questionId,$module_id,$question_order_id,$text, $text_1);
        } 
         
        
     }

    private function save_student_answer($module_id,$question_order_id)
    {
        $ans_array = $this->session->userdata('data');
        
        if(!is_array($ans_array)) $ans_array = array();
        
        $question_info_ai = $this->tutor_model->getModuleQuestion($module_id,$question_order_id,null);
        
        $ind_ans = array( 'question_order_id'=>$question_info_ai[0]['question_order'], 'module_type'=>$question_info_ai[0]['moduleType'], 'module_id'=>$question_info_ai[0]['module_id'],'question_id'=>$question_info_ai[0]['question_id']);
        
        $ans_array[$question_order_id] = $ind_ans;
        
        $this->session->set_userdata('data',$ans_array);
        
        $total_ans = $this->session->userdata('data',$ans_array);
                
        $data['st_ans'] = json_encode($total_ans);
        
        $data['st_id'] = $this->session->userdata('user_id');
        
        $data['module_id'] = $module_id;
        
        $this->db->insert('tbl_student_answer', $data);
        
        $this->session->unset_userdata('data',$ans_array);
        echo 6;
        
    }
}
