<?php

class Upper_Level extends CI_Controller{
    public function __construct() {
        parent::__construct();
        
         $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('userType');
		//echo $user_id; echo '<br>';echo  $user_type;die;
        if($user_id == NULL && $user_type == NULL){
            redirect('welcome');
        }
        if($user_type != 2){
            redirect('welcome');
        }
        
        $this->load->model('tutor_model');
        
    }
    
    public function index() {
        $data['page_title'] = '.:: Q-Study :: Tutor yourself...';
        $data['headerlink'] = $this->load->view('dashboard_template/headerlink', $data, true);
        $data['header'] = $this->load->view('dashboard_template/header', $data, true);
        $data['footerlink'] = $this->load->view('dashboard_template/footerlink', $data, true);
        
        $data['maincontent'] = $this->load->view('upper_level/upper_level_dashboard', $data, TRUE);
        $this->load->view('master_dashboard', $data);
        
    }
}
